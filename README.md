# BetterComments

Mediawiki extension.

## Description

* Version 0.3
* Extension makes easier creating comments at discussion pages.
* No database needed, comments remains editable the old way too. It's just an javascript enhancement.
* Fires at disscussion pages and pages listed in _extension.json_ (extraPagesAllowed).

## Installation

* Make sure you have MediaWiki 1.39+ installed.
* Bootstrap 5 support required.
* Download and place the extension to your _/extensions/_ folder.
* Add the following code to your LocalSettings.php: `wfLoadExtension( 'BetterComments' )`;

## Internationalization

This extension is available in English and Czech language. For other languages, just edit files in /i18n/ folder.

## Poznámky k realizaci

* Pokud jde o diskusní stránku, přidáme na začátek button se sbaleným formulářem pro přidání nového komentáře na začátek (https://getbootstrap.com/docs/5.0/components/collapse/)
* Taky projde wiki text a na konec každého řádku, po kterém následuje ":" přidá tag "<span class='d-none bcanswer'>cindent=0,cpos=$commentPosition,tpos=$threadPosition" . "</span>".
* Javascript pak zařídí nahrazení těchto tagů buttonem se sbaleným formulářem a zařídí vlastní aktualizaci stránky.
* Aktualizace bude probíhat skrze https://www.mediawiki.org/wiki/API:Edit.
* Kam tu odpověď zapasovat, můžeme určit např. podle pořadí výskytu úvodního ":" na stránce.
* Po odeslání komentáře refreshujeme stránku, ať je změna vidět.
* Pokud dojde při odeslání k editačnímu konfliktu, dáme vědět uživateli, že je ve válce a bude muset použít CTRL-V, aby si ten komentář prosadil. Po zavření infookna se refreshuje stránka a user musí odpovědět znova.

## RELEASE NOTES

### 0.3

* Convert style to Bootstrap 5

## Authors and license

* [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart)
* MIT License, Copyright (c) 2023 First Faculty of Medicine, Charles University
