<?php

/**
 * All hooked functions used by BetterComments
 * @ingroup Extensions
 * @author Josef Martiňák
 * @license MIT
 */

class BetterCommentsHooks {

	/**
	 * Adds <answerComment/> tags at the end of comments
	 * @param object $parser: instance of Parser
	 * @return Boolean: true
	 */
	public static function addTags( &$parser, &$text, &$strip_state ) {

		$config = RequestContext::getMain()->getConfig();
		$title = $parser->getTitle();

		// run only at discussion or forum pages
		if(!$title->isTalkPage()) {
			if(!in_array($title->getArticleId(), $config->get("extraPagesAllowed"))) return true;
		}

		$out = '';
		$commentPosition = 0;
		$threadPosition = 0;
		$threads = preg_split('/\n(={2}[^=]*={2})\n/', $text, -1, PREG_SPLIT_DELIM_CAPTURE);
		foreach($threads as $thread) {
			if($out == '') {
				// before first thread
				$out = $thread;
				continue;
			}

			if(preg_match('/={2}([^=]*)={2}/', $thread, $m)) {
				// header
				$out .= $thread . PHP_EOL;
				if(!in_array(trim($m[1]), ['Obsah', 'Contents'])) $threadPosition++;
			}
			else {
				// thread's content
				$threadContent = '';
				foreach(preg_split('/((\r?\n)|(\r\n?))/', $thread) as $line){
					if(preg_match('/^(:+)(.*$)/', $line, $m)) {
						// comment line
						$commentPosition++;
						$threadContent .= rtrim($line) . " <span class=\"d-none bcanswer\" data-cindent=\"" . strlen($m[1]) . "\" data-cpos=\"$commentPosition\" data-tpos=\"$threadPosition\"></span>\n";
					}
					elseif(!empty($line)) $threadContent .= trim($line) . "\n";
					else $threadContent .= "\n";
				}

				if(preg_match('/\n:/', $threadContent)) {
					// add answer btn first level
					$threadContent = preg_replace("/\n:/", "<span class=\"d-none bcanswer\" data-cindent=\"0\" data-cpos=\"0\" data-tpos=\"$threadPosition\"></span>\n:",$threadContent, 1);
				}
				else {
					// no responses in thread ... add answer btn
					$threadContent = rtrim($threadContent) . "<span class=\"d-none bcanswer\" data-cindent=\"0\" data-cpos=\"0\" data-tpos=\"$threadPosition\"></span>\n";
				}
				$out .= rtrim($threadContent) . "\n\n";
			}
		}
		$text = $out;
		return true;
	}

	/**
	 * Add "create new thread" button
	 * @param object $out: instance of OutputPage
	 * @param object $skin: instance of Skin, unused
	 */
	public static function addButtons( &$out, &$skin ) {

		if( !$out->isArticle() ) return true;
		$title = $out->getTitle();

		// run only at discussion or forum pages
		$config = $out->getConfig();

		if(!$title->isTalkPage()) {
			if(!in_array($title->getArticleId(), $config->get("extraPagesAllowed"))) return true;
		}

		$out->addModules('ext.BetterComments');

		$alerts = "<div id='bcDanger' class='alert alert-danger d-none mt-3 mb-3' role='alert'><br><button class='btn btn-secondary btn-sm refreshBtn mt-2'>" . wfMessage( "bettercomments-refresh" )->plain() . "</button></div>\n";
		$alerts .= "<div id='bcSuccess' class='alert alert-success d-none mt-3 mb-3' role='alert'></div>\n";

		$form = "<div class='mb-4'><p>\n";
		$form .= "<button class='btn btn-primary' id='newThreadBtn' type='button' data-bs-toggle='collapse' data-bs-target='#newThread' aria-expanded='false' aria-controls='newThread'>";
		$form .= wfMessage( "bettercomments-newthread" )->plain() . "</button>\n";
		$form .= "</p>\n";
		$form .= "<div class='collapse' id='newThread' data-shownTS=''>\n";
		$form .= "<form id='newThreadForm'><div class='form-group'>\n";
		$form .= "<input class='form-control mb-2' type='text' id='newThreadName' placeholder='" . wfMessage( "bettercomments-name" )->plain() . "' required>\n";
		$form .= "<textarea class='form-control mb-2' style='height:200px;' id='newThreadText' placeholder='" . wfMessage( "bettercomments-text" )->plain() . "' required></textarea>\n";
		$form .= "<button class='btn btn-primary me-2' type='submit' id='threadSubmitBtn'>";
		$form .= wfMessage( "bettercomments-save" )->plain() . "</button>\n";
		$form .= "<button class='btn btn-secondary cancelBtn' type='button'>" . wfMessage( "bettercomments-cancel" )->plain() . "</button>";
		$form .= "</div></form>\n";
		$form .= "</div></div>\n";
		if(preg_match("/<h2>.*?<\/h2>/", $out->mBodytext)) {
			// append new thread form
			$out->mBodytext = preg_replace("/<h2>(?!Obsah|Contents)/", "<br>" . $alerts . $form . "<h2>", $out->mBodytext, 1);
			// replace #bcanswer with buttons
			$out->mBodytext = preg_replace("/<span class=\"d-none bcanswer\" data-cindent=\"([0-9]*)\" data-cpos=\"([0-9]*)\" data-tpos=\"([0-9]*)\"><\/span>/", 
				"<button class='btn btn-link ps-0 my-0 py-0 ms-1' type='button' data-bs-toggle='collapse' data-bs-target='#newComment$1$2$3' aria-expanded='false' aria-controls='newComment'>"
				. wfMessage( "bettercomments-answer" )->plain() . "</button><div class='collapse newComment mt-0 mb-0' id='newComment$1$2$3' data-shownTS='' data-cindent='$1' data-cpos='$2' data-tpos='$3'>\n<form class='newCommentForm'>
				<div class='form-group'>\n<textarea class='form-control mb-2 newCommentText' style='height:200px;' placeholder='" . wfMessage( "bettercomments-text" )->plain() . "' required></textarea>\n
				<button class='btn btn-primary me-2' type='submit'>" . wfMessage( "bettercomments-save" )->plain() . "</button><button class='btn btn-secondary cancelBtn' type='button'>" . wfMessage( "bettercomments-cancel" )->plain() . "</button>\n</div></form>\n</div>\n",
				$out->mBodytext);
		}
		else {
			// no threads in discussion, append at the end
			$out->mBodytext .= $alerts . $form;
		}

		return true;
	}
}